import React,{Component,Children} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Alert, PermissionsAndroid,
} from "react-native";

import {SafeAreaProvider} from "react-native-safe-area-context";

import Icon from "react-native-vector-icons/Ionicons";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";

import { NavigationContainer} from "@react-navigation/native";
import { CardStyleInterpolators, createStackNavigator, TransitionPresets} from "@react-navigation/stack";
import AnimatedSplash from "react-native-animated-splash-screen";

import AsyncStorage from "@react-native-community/async-storage";



import HomeScreen from "./Pages/HomePage";
import ProfileScreen from "./Pages/ProfilePage";
import SearchScreen from "./Pages/SearchPage";
import AboutScreen from "./Pages/PagesInsideProfilePage/AboutPage";
import SettingsScreen from "./Pages/PagesInsideProfilePage/SettingsPage";
import FavoriteDrinksScreen from "./Pages/PagesInsideProfilePage/FavoriteDrinksPage";
import CustomerSupportScreen from "./Pages/PagesInsideProfilePage/CustomerSupportPage";
import RegisterScreen from "./Pages/PagesInsideProfilePage/PagesWithAuthentication/RegisterPage";
import LoginScreen from "./Pages/PagesInsideProfilePage/PagesWithAuthentication/LoginPage";
import BarProfileScreen from "./Pages/BarProfilePage";
import DrinkProfileScreen from "./Pages/DrinkProfilePage";
import FavoriteBarsScreen from './Pages/PagesInsideProfilePage/FavoriteBarsPage';

import {AuthContext} from "./AuthContext";
import StackNavigator from "@react-navigation/stack/src/navigators/createStackNavigator";


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const AuthStack = createStackNavigator();



const TransitionScreenOptions = {
  ...TransitionPresets.SlideFromRightIOS,
};




function MainStack(){

  return (
    <SafeAreaProvider>
    <NavigationContainer>
      <Stack.Navigator mode={"modal"} screenOptions={TransitionScreenOptions}>
        <Stack.Screen options={{headerShown: false}} name="Root" component={BottomNavigation} />
        <Stack.Screen options={{gestureEnabled: true, headerTitleAlign: "center", title: "Favorite drinks"}} name="FavoriteDrinks" component={FavoriteDrinksScreen} />
        <Stack.Screen options={{gestureEnabled: true, headerTitleAlign: "center", title: "Favorite bars"}} name="FavoriteBars"
                      initialParams={{drinkId: null}}
                      component={FavoriteBarsScreen} />
        <Stack.Screen options={{gestureEnabled: true, headerTitleAlign: "center", title: "Customer support" }} name="CustomerSupport" component={CustomerSupportScreen} />
        <Stack.Screen options={{gestureEnabled: true, headerTitleAlign: "center", title: "About"}} name="About" component={AboutScreen} />
        <Stack.Screen options={{gestureEnabled: true, headerTitleAlign: "center", title: "Settings"}} name="Settings" component={SettingsScreen} />
        <Stack.Screen  options={{
          gestureEnabled: true,
          headerShown: false,
          cardOverlayEnabled: true,
          headerTransparent: true,
          title: "Bar profile",
          gestureDirection: "vertical",
          cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
          gestureResponseDistance: {vertical: 250}

        }} name="BarProfile" component={BarProfileScreen} />
        <Stack.Screen  options={{
          gestureEnabled: true,
          headerShown: false,
          cardOverlayEnabled: true,
          headerTransparent: true,
          title: "Drink profile",
          gestureDirection: "vertical",
          cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
          gestureResponseDistance: {vertical: 250}

        }} name="DrinkProfile" component={DrinkProfileScreen} />
      </Stack.Navigator>
    </NavigationContainer>
    </SafeAreaProvider>
  );
}

function AuthenticationStack() {
  return (
    <SafeAreaProvider>
    <NavigationContainer>
      <AuthStack.Navigator mode={"modal"} initialRouteName={"Login"} screenOptions={{
        headerShown: false
      }}>
        <AuthStack.Screen name="Login" component={LoginScreen} />
        <AuthStack.Screen name="Register" options={{
          gestureEnabled: true,
          headerShown: false,
          headerTransparent: true,
          title: "Drink profile",
          gestureDirection: "vertical",
          cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
          gestureResponseDistance: {vertical: 250}

        }} component={RegisterScreen} />
      </AuthStack.Navigator>
    </NavigationContainer>
    </SafeAreaProvider>
  );
}


function BottomNavigation(){
  return(
    <Tab.Navigator initialRouteName={"Home"}  tabBarOptions={{showLabel: false,style:styles.barStyle}} >
      <Tab.Screen name={"Profile"} component={ProfileScreen}
                           options={{
                             tabBarIcon: ({ focused }) => (
                                          <Icon color={!focused ? "#878787" : "black"} size={25} name={!focused ? "person-outline" : "person"} />
        )}}/>
      <Tab.Screen name={"Home"} component={HomeScreen}
                            initialParams={{ markers: [], favoriteBar: null, markersObtained: null, favoriteBarObtained: null}}
                            options={{
                             tabBarIcon: ({ focused }) => (
                                 <Icon color={!focused ? "#878787" : "black"} size={25} name={!focused ? "home-outline" : "home-sharp"} />
                             )}}/>
      <Tab.Screen name={"Search"} component={SearchScreen}
                           options={{
                             tabBarIcon: ({focused}) => (
                                 <Icon color={!focused ? "#878787" : "black"} size={25} name={!focused ? "search-outline" : "search"} />
                             )}}/>
    </Tab.Navigator>
  );
};





async function requestLocationPermission()
{
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Example App',
        'message': 'Example App access to your location'
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      //console.log("You can use the location")
    } else {
      console.log("Location permission denied")
    }
  } catch (err) {
    console.warn(err)
  }
}





const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  barStyle: {
    height: "8%",
    borderColor: "black",
    borderTopWidth: 0.25,
    backgroundColor: 'white',


  }
});





export default class App extends Component{

  toggleLogged = loggedIn => {
    this.setState({ loggedIn: loggedIn });
  };


  state = {
    loggedIn: false,
    toggleLogged: this.toggleLogged
  }

  async componentDidMount() {
    await requestLocationPermission();
    const token = await AsyncStorage.getItem("AccessToken");
    if(token){
      this.setState({loggedIn: true})
    }
  }


  render() {
    return(

      <AuthContext.Provider value={this.state}>
        {this.state.loggedIn ? (
        <MainStack/>
      ) : (
        <AuthenticationStack/>)}
      </AuthContext.Provider>
    );
  }



}
