import React,{Component} from 'react';

import {PermissionsAndroid} from "react-native";
import GetLocation from 'react-native-get-location';


export default async function getLocation(){
    let Coords;

    await GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 15000,
    })
        .then(location => {
            Coords = location;
        })
        .catch(error => {
            const { code, message } = error;
            console.warn(code, message);
        })

    return Coords;

}
