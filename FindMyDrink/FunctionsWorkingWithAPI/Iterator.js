

export default function createIterator(array) {
    let index = 0;
    let max = array.length -1;

    const iterator = {
        next: function() {
            if (index < max) {
                index++;
                return index;
            }
            index = 0;
            return index;
        },
        previous: function (){
            if (index > 0){
                index--;
                return index;
            }
            index = max
            return index;
        },
        current: function (){
            return index;
        },
        update: function(num){
            if(num >= 0 && num <= max ){
                index = num;
                return index;
            }
        }
    };
    return iterator;
}
