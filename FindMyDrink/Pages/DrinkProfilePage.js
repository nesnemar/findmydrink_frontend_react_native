import React,{Component} from 'react';

import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Dimensions, TouchableHighlight, TextInput, RefreshControl, ImageBackground, TouchableOpacity
} from 'react-native';

import {SafeAreaView} from "react-native-safe-area-context";

import Icon from "react-native-vector-icons/Ionicons";

import {AirbnbRating} from "react-native-ratings";

import { Button } from "react-native-paper";

import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";

import {Card, CardItem, Spinner} from 'native-base';
import CustomAlert from '../CustomAlert';
import AwesomeAlert from 'react-native-awesome-alerts';

import {remote} from "../herokuAddress";
import {BlurView} from 'expo-blur';



export default class DrinkProfileScreen extends Component {

  state: {
    drinkLoaded: null,
    refreshing: false,
    //justDrink
    drink: null,
    drinkRating: 0,
    favorite: null,

    //myOwn
    myReviewComment: "",
    myReviewRating: null,
    reviewState: "NOT PRESENT",
    prevRating: null,
    prevComment: "",
    //fromOthers
    reviews: null,

    message: null
  }

  constructor(props) {
    super(props);
    this.state = {
      drink: null,
      drinkLoaded: "LOADING",
      refreshing: false,
      reviews: [],

      myReviewComment: "",
      myReviewRating: 0,
      reviewState: "NOT PRESENT",
      prevRating: 0,
      prevComment: "",

      message: null
    }

  }


  async componentDidMount() {
    await this.getDrinkInfo();
  }

  async getDrinkInfo(){

    let data = {
      drink: {
        drinkId: null,
        name: null,
        price: null,
      },
      drinkRating: 0,
      reviews: [],
      myReview: null,
      favorite: null, //bool
      drinkLoaded: null
    }


    console.log(this.props.route.params);

    await axios.post(remote +'/api/bars/drinkprofile', {barId: this.props.route.params.barId, drinkId: this.props.route.params.drinkId }, { headers: { "Authorization": await AsyncStorage.getItem("AccessToken") } })
      .then((response) => {

        console.log(response.data)

        data.drink.drinkId = response.data._id;
        data.drink.globalDrinkId = response.data.drink_id;
        data.drink.price = response.data.price;
        data.drink.name = response.data.name;
        data.drinkRating = response.data.rating;

        data.myReview = response.data.my_review;
        data.reviews = response.data.reviews;
        data.favorite = response.data.favourite;


        this.setState({
          drinkLoaded: "OK",
          drinkRating: data.drinkRating,
          drink: data.drink,
          favorite: data.favorite,
          reviews: data.reviews,

          myReviewComment: data.myReview ? (data.myReview.comment) : (""),
          myReviewRating: data.myReview ? (data.myReview.rating) : (0),
          reviewState: data.myReview ? ("PRESENT") : ("NOT PRESENT"),
          prevComment: data.myReview ? (data.myReview.comment) : (""),
          prevRating: data.myReview ? (data.myReview.rating) : (0),

        })

      })
      .catch(async(error) => {
        await this.setState({
          drinkLoaded: "ERROR",
          reviewState: "NOT PRESENT"
        })
      })
  }


  async changeFavoriteState(){
    await axios.post(remote +'/api/bars/addfavdrink', {barId: this.props.route.params.barId, drinkId: this.state.drink.globalDrinkId }, { headers: { "Authorization": await AsyncStorage.getItem("AccessToken") } })
      .then((response) => {

        this.setState({
          drinkLoaded: "OK",
          favorite: response.data.fav
        })

      })
      .catch(async(error) => {
        await this.setState({
          drinkLoaded: "ERROR"
        })
      })
  }

  async sendReview(){

    let review = this.state.myReviewComment.trim();
    let rating = this.state.myReviewRating;

    if(review.length === 0){

      this.setState({message: "Please fill review input field!"/*, myReviewComment: this.state.prevComment*/})

    } else if(rating === 0){

      this.setState({message: "Please rate this drink!", myReviewRating: this.state.prevRating})

    } else {

      await axios.post(remote +'/api/bars/addreview', {
        comment: review,
        rating: rating,
        barId: this.props.route.params.barId,
        drinkId: this.state.drink.drinkId,
        globalDrinkId: this.state.drink.globalDrinkId
      }, {headers: {"Authorization": await AsyncStorage.getItem("AccessToken")}})
          .then(async (response) => {

            console.log(response.data)

            await this.setState({
              myReviewComment: response.data ? (response.data.comment) : (""),
              myReviewRating: response.data ? (response.data.rating) : (0),
              reviewState: response.data ? ("PRESENT") : ("NOT PRESENT"),
              prevComment: response.data ? (response.data.comment) : (""),
              prevRating: response.data ? (response.data.rating) : (0)
            })

          })
          .catch(async (error) => {
            await this.setState({
              reviewState: "NOT PRESENT",
              myReviewComment: this.state.prevComment,
              myReviewRating: this.state.prevRating,
            })
          })
    }

    await this.getDrinkRating();

  }

  async deleteReview(){
    await axios.post(remote +'/api/bars/deletereview', {barId: this.props.route.params.barId, drinkId: this.state.drink.drinkId}, { headers: { "Authorization": await AsyncStorage.getItem("AccessToken") } })
        .then(async (response) => {

          if(response.status === 200) {
            await this.setState({
              myReviewComment: "",
              myReviewRating: 0,
              reviewState: "NOT PRESENT",
              prevRating: 0,
              prevComment: "",
              message: null
            })
          }

        })
        .catch(async(error) => {
          this.setState({
            message: "A problem occured while deleting this review..."
          })
        })

    await this.getDrinkRating();

  }


  async getDrinkRating(){

    await axios.post(remote +'/api/bars/getdrinkrating', {barId: this.props.route.params.barId, drinkId: this.state.drink.drinkId }, { headers: { "Authorization": await AsyncStorage.getItem("AccessToken") } })
        .then(response => {
          console.log(response.data)
          this.setState({drinkRating: response.data.rating})
        })
        .catch(error => {
          console.log(error)
        })

  }




  render() {
    if (this.state.drinkLoaded === "OK") {
      return (
        <ScrollView style={styles.container} keyboardShouldPersistTaps={"always"}>

          <StatusBar
            animated={true}
            backgroundColor={"transparent"}
            barStyle="light-content"
            translucent={true}

          />


            <View style={styles.graphics}>
              <ImageBackground style={{width: "100%", height: "100%"}} source={require("../Resources/drink.jpg")}>


                <View style={{position:"absolute", top:10, left:10}}>
                  <AirbnbRating
                      defaultRating={this.state.drinkRating}
                      showRating={false}
                      size={25}
                      count={5}
                      isDisabled={true}
                  />
                </View>

                <TouchableHighlight style={{position:"absolute", top: 10, right:10}}>
                  <View>
                    <Icon color={"red"} size={37} name={ this.state.favorite === true ? ("heart") : ("heart-outline") } onPress={() => this.changeFavoriteState()}/>
                  </View>
                </TouchableHighlight>

                <BlurView
                    tint={"dark"}
                    intensity={75} style={styles.blurView}>
                  <Text style={styles.drinkInfo}>{this.state.drink.name}</Text>
                  <Text style={styles.drinkInfo}>{this.state.drink.price + " Kč"}</Text>
                </BlurView>
              </ImageBackground>
            </View>


            <ScrollView style={styles.ReviewSection} keyboardShouldPersistTaps={"always"}>

              <View style={{paddingHorizontal: 10}}>
                <View style={styles.userReviewRating}>
                  <AirbnbRating isDisabled={this.state.reviewState !== "NOT PRESENT" && this.state.reviewState !== "EDITING" ? true : false}
                                defaultRating={this.state.myReviewRating}

                                size={20}
                                showRating={false}
                                count={5}
                                onFinishRating={(myReviewRating) => this.setState({ myReviewRating })}
                  />
                </View>

                <TextInput
                  defaultValue={this.state.myReviewComment}
                  onChangeText={(myReviewComment) => this.setState({ myReviewComment })}
                  placeholder='Write your review...'
                  style={[styles.reviewInput,{maxHeight: this.state.reviewState !== "PRESENT" ? 100 : null}]}
                  scrollEnabled={true}
                  multiline={true}
                  editable={this.state.reviewState === "PRESENT" ? (false) : (true)}
                />

                {this.state.reviewState !== "PRESENT" ? (
                  <Icon style={styles.sendIcon}
                        size={25} color={"black"}
                        name={"send-sharp"}
                        onPress={() => this.sendReview()}
                  />
                  ) : (null)}
              </View>

              <View style={styles.buttonContainer}>

                {this.state.reviewState === "EDITING" ? (
                    <>
                      <TouchableOpacity
                          onPress={() => this.setState({reviewState: "DELETING", message: "Are you sure, You want to delete this review?"})}>
                        <Text style={styles.buttonLabel}>Delete</Text>
                      </TouchableOpacity>

                      <TouchableOpacity
                          onPress={()=>this.setState({reviewState: "PRESENT", myReviewRating: this.state.prevRating, myReviewComment: this.state.prevComment})}                      >
                        <Text style={styles.buttonLabel}>Cancel</Text>
                      </TouchableOpacity>
                    </>
                ) : this.state.reviewState === "PRESENT" ? (
                    <>
                      <TouchableOpacity onPress={() => this.setState({reviewState: "EDITING"})}>
                        <Text style={styles.buttonLabel}>Edit</Text>
                      </TouchableOpacity>
                    </>
                ) : (null)}

              </View>


              <Card transparent={true} style={styles.otherReviews}>

                <Text style={styles.commentHeader}>User reviews</Text>

                { this.state.reviews.length !== 0 ? (

                    this.state.reviews.map((review: any) => (

                        <CardItem key={review.comment} style={styles.cardItem}>
                          <Text style={styles.author}>{review.user_name}</Text>
                          <View style={styles.userRating}>
                            <AirbnbRating isDisabled={true} size={15} defaultRating={review.rating} showRating={false} count={5}/>
                          </View>
                          <View style={styles.userComment}>
                            <Text style={{fontSize: 17}}>{review.comment}</Text>
                          </View>
                        </CardItem>
                    )))
                    : (
                        <Text style={{paddingHorizontal: 20, fontSize: 15, fontWeight: Platform.OS === "ios" ? ("800") : ("bold")}}>
                          No reviews yet...
                        </Text>
                        )}
              </Card>

            </ScrollView>

            <AwesomeAlert
                show={this.state.message  || (this.state.reviewState === "DELETING" && this.state.message) ? (true) : (false)}
                customView={CustomAlert(this.state.message)}
                closeOnHardwareBackPress={false}
                closeOnTouchOutside={false}
                confirmText={"OK"}
                showConfirmButton={true}
                showCancelButton={this.state.reviewState === "DELETING" ? (true) : (false)}
                onCancelPressed={() => this.setState({message: null, reviewState: "PRESENT"})}
                onConfirmPressed={() => {this.state.reviewState === "DELETING" ? (this.deleteReview()) : (this.setState({message: null}))}}
            />


        </ScrollView>
      )
    } else {
      return (
          <View style={styles.spinner}>
            <Spinner color={"black"}/>
          </View>
      )
    }
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: "white"
  },

  graphics: {
    height: 200
  },

  blurView: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    display: 'flex',
    flexDirection:"row",
    justifyContent: "space-between"
  },

  drinkInfo: {
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold"),
    fontSize: 20,
    color: "white",
    padding: 3
  },


  ReviewSection:{
    marginTop: "5%",
  },
  commentHeader: {
    marginTop: "7.5%",
    marginBottom: "5%",
    fontSize: 20,
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold"),
    marginLeft: "5%"

  },

  userReviewRating: {
    alignItems: "center"
  },

  reviewInput: {
    paddingLeft: 5,
    paddingRight: 40,
    paddingVertical: 5,
    borderBottomWidth: 0.5,
    borderColor: "grey",
    fontSize: 17,
    width: "100%",
    color: "black"
  },

  buttonContainer: {
    marginTop: 15,
    display: "flex",
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-around"
  },

  buttonLabel: {
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold"),
    fontSize: 15
  },

  sendIcon: {
    position:"absolute",
    bottom: 5,
    right: 5
  },


  otherReviews: {
    marginTop: "5%"
  },

  cardItem: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    borderTopWidth: 0.5,
    borderColor: "#DCDCDC",
  },
  author: {
    paddingVertical: 5,
    fontSize: 17.5,
    color: "black",
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold")
  },
  userRating: {
    paddingVertical: 7
  },
  userComment: {
    paddingHorizontal: "2.5%",
    paddingVertical: 7,
    backgroundColor: "#EBEBEB",
    borderRadius: 10,
    width: "100%"
  },

  spinner: {
    flex: 1,
    display: "flex",
    alignItems:"center",
    justifyContent:"center"
  }

});
