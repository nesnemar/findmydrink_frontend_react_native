import React,{Component} from 'react';

import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
    ImageBackground,
  Dimensions, RefreshControl,
} from 'react-native';

import {SafeAreaView} from "react-native-safe-area-context";

import { Button } from "react-native-paper";

import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";

import { Card, CardItem, Spinner} from "native-base";
import {remote} from "../herokuAddress";

import Icon from "react-native-vector-icons/Ionicons";

import {BlurView} from 'expo-blur';

export default class BarProfileScreen extends Component {

  state: {
    bar: null,
    barLoaded: null,
    refreshing: false
  }

  constructor(props) {
    super(props);
    this.state = {
      bar: null,
      barLoaded: "LOADING",
      refreshing: false
    }


  }


  async componentDidMount() {
    await this.getBarInfo();
  }

  async getBarInfo(){

    let data = {
      bar: {
        barId: null,
        name: null,
        menu: null,
        opening_hours: null
      },
      barLoaded: null
    }


      await axios.post(remote + '/api/bars/profilebar', { barId: this.props.route.params.id }, { headers: { "Authorization": await AsyncStorage.getItem("AccessToken") } })
        .then((response) => {

          console.log(response)

          data.bar.barId = response.data._id;
          data.bar.name = response.data.name;
          data.bar.menu = response.data.menu;
          data.bar.opening_hours = response.data.opening_hours;


          this.setState({
            barLoaded: "OK",
            bar: data.bar })

        })
        .catch((error) => {
          this.setState({
            barLoaded: "ERROR"
          })
        })
  }

  showDrinkScreen(DrinkID){
    this.props.navigation.navigate('DrinkProfile',{barId: this.state.bar.barId,drinkId : DrinkID})
  }


  render() {
    if (this.state.barLoaded == "OK") {
      return (
        <ScrollView style={styles.container}>

          <StatusBar
            animated={true}
            backgroundColor={"transparent"}
            barStyle="light-content"
            translucent={true}

          />

          <View style={styles.graphics}>
            <ImageBackground style={styles.barImage} source={require("../Resources/bar.jpg")}>
              <BlurView
                  tint={"dark"}
                  intensity={75} style={{position: "absolute", bottom: 0, left: 0}}>
                <Text style={styles.barName}>{this.state.bar.name}</Text>
              </BlurView>
            </ImageBackground>
          </View>


          <View style={styles.OpeningTimes}>
              <Text style={styles.sectionHeader}>Opening times</Text>


            <View style={styles.timesGroup}>
              <Text style={styles.openingInterval}>{"Mon: " + "12:00" + " - " + "24:00"}</Text>
              <Text style={styles.openingInterval}>{"Tue: " + "12:00" + " - " + "24:00"}</Text>
              <Text style={styles.openingInterval}>{"Wed: " + "12:00" + " - " + "24:00"}</Text>
              <Text style={styles.openingInterval}>{"Thu: " + "12:00" + " - " + "24:00"}</Text>
              <Text style={styles.openingInterval}>{"Fri: " + "12:00" + " - " + "24:00"}</Text>
              <Text style={styles.openingInterval}>{"Sat: " + "12:00" + " - " + "24:00"}</Text>
              <Text style={styles.openingInterval}>{"Sun: " + "12:00" + " - " + "24:00"}</Text>

            </View>

          </View>



          <View style={styles.Menu}>

            <Text style={styles.sectionHeader}>Menu</Text>

            <Card transparent={true}>
            {Object.entries(this.state.bar.menu).map((category: any) => (
              <View key={category[0]}>
                <Text style={styles.category}>{category[0]}</Text>
                {category[1].map((drink) => (
                  <CardItem key={drink._id} style={styles.cardItem} button onPress={() => this.showDrinkScreen(drink._id)}>
                    <Text style={[styles.drinkID, { color: this.props.route.params.drinkId === drink._id ? ("red") : ("black") }]}>{drink.name}</Text>
                    <Text style={styles.drinkPrice}>{drink.price + " Kč"}</Text>
                  </CardItem>
                ))}
              </View>
            ))}
            </Card>

          </View>

    </ScrollView>
      )
    } else {
      return (
        <View style={styles.spinner}>
          <Spinner color={"black"}/>
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },

  graphics: {
    height: 200
  },

  barImage:{
    width: "100%",
    height: "100%"
  },

  barName: {
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold"),
    fontSize: 20,
    color: "white",
    padding: 5
  },

  sectionHeader: {
    fontSize: 22,
    textAlign: "center",
    marginTop: "7.5%",
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold")
  },



  OpeningTimes: {

  },

  timesGroup: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    alignItems: "center",
    marginVertical: 20
  },

  openingInterval: {
    padding: 5,
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold")
  },



  Menu:{

  },

  category: {
    marginBottom: "2.5%",
    marginTop: "7.5%",
    fontSize: 18,
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold"),
    paddingLeft: "2.5%"

  },
  cardItem: {
    paddingLeft: "5%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  drinkID: {
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold")
  },
  drinkPrice: {
  },


  spinner: {
    flex: 1,
    display: "flex",
    alignItems:"center",
    justifyContent:"center"
  }


});
