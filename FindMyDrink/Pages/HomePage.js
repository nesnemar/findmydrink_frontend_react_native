import React,{Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity
} from "react-native";


import Icon from "react-native-vector-icons/Ionicons";

import MapView,{ PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps';

import { Button } from "react-native-paper";

import Spinner from "react-native-spinkit"
import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";
import Geolocation from "react-native-geolocation-service";
import {AirbnbRating} from "react-native-ratings";

import BottomSheet from 'reanimated-bottom-sheet';

import getMap from "../FunctionsWorkingWithAPI/getMapStyle";

import {remote} from "../herokuAddress";

import createIterator from '../FunctionsWorkingWithAPI/Iterator';
import getLocation from '../FunctionsWorkingWithAPI/getLocation';



export default class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.map = React.createRef();
    this.modal = React.createRef();
    this.selectedMarker = React.createRef();
    this.iterator = null;
  }

  state = {
    isMapLoaded: false,
    region: {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0,
      longitudeDelta: 0,
    },
    bar: {
      id: null,
      name: null,
      rating: null,
      address: {
        street: null,
        buildingNumber: null,
        city: null,
        postal_code: null
      }
    },

    modalLoaded: null,
    iteratorCount: null,
    maxCount: null,

    favoriteBar: null

  }


  async componentDidMount() {
    let location = await getLocation();
    await this.setState({
      isMapLoaded: true, region: {
        latitude: location.latitude, longitude: location.longitude, latitudeDelta: 0.0011,
        longitudeDelta: 0.0021
      }
    })
  }

  async componentDidUpdate() {
    if (this.props.route.params.favoriteBarObtained === "SENT") {
      this.props.route.params.favoriteBarObtained = null;
      await this.setState({favoriteBar: this.props.route.params.favoriteBar, iteratorCount: null, maxCount: null});
      let bar = this.state.favoriteBar;
      this.animateToCoords(bar.coordinates[1], bar.coordinates[0])
      this.openModal(bar);
      this.props.route.params.markers = [];
      this.iterator = null;
    }

    else if (this.props.route.params.markersObtained === "SENT") {
      if (this.props.route.params.markers.length > 0) {
        this.props.route.params.markersObtained = null;
        this.iterator = createIterator(this.props.route.params.markers);
        this.setState({iteratorCount: 0, maxCount: this.props.route.params.markers.length - 1, favoriteBar: null});
        let marker = this.props.route.params.markers[0];
        this.animateToCoords(marker.latitude, marker.longitude);
        this.openModal(marker)
      }
    }
  }


  async loadLocation() {
    await Geolocation.getCurrentPosition(location => {
          this.setState({
            region: {
              latitude: location.coords.latitude,
              longitude: location.coords.longitude,
              latitudeDelta: 0.0011,
              longitudeDelta: 0.0021,
            },
            isMapLoaded: true
          })
        }, (error) => this.setState({error: error.message}),
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000})
  }


  async animate() {
    await this.loadLocation();
    this.animateToCoords(this.state.region.latitude, this.state.region.longitude)
  }


  async openModal(marker) {
    await this.getDataForModal(marker);
    this.modal.snapTo(0);

  }

  async getDataForModal(marker) {

    let data = {
      barId: marker.barId,
      barName: null,
      address: {
        street: null,
        buildingNumber: null,
        city: null,
        postal_code: null
      },
      drinkMenuId: marker.drinkMenuId,
      globalDrinkId: null,
      drinkName: null,
      price: null,
      imageUrl: null,
      drinkRating: null,

      fmdRating: marker.fmdRating
    }

    await axios.post(remote + '/api/bars/modalbar', {
      barId: data.barId,
      drinkMenuId: data.drinkMenuId
    }, {headers: {"Authorization": await AsyncStorage.getItem("AccessToken")}})
        .then(async (response) => {

          data.barName = response.data.barName;
          data.address.buildingNumber = response.data.address.buildingNumber;
          data.address.street = response.data.address.street;
          data.address.postal_code = response.data.address.postalCode;
          data.address.city = response.data.address.city;

          data.drinkName = response.data.drinkName;
          data.price = response.data.price;
          data.imageUrl = response.data.imageUrl;
          data.drinkRating = response.data.drinkRating;


          console.log(data)

          await this.setState({
            modalLoaded: "OK",
            bar: data
          })
        })
        .catch(async (error) => {
          console.log(error);
          await this.setState({
            modalLoaded: "ERROR"
          })
        })
  }

  findIndex(barID) {
    let array = this.props.route.params.markers;
    if (array.length > 0) {
      for (let i = 0; i < array.length; i++) {
        if (array[i].barId === barID) {
          return i;
        }
      }
    }

    return 0;
  }

  iterateThroughMarkers(ascending) {

    if (this.iterator != null) {
      let marker = null;

      if (ascending) {
        marker = this.props.route.params.markers[this.iterator.next()];
      } else {
        marker = this.props.route.params.markers[this.iterator.previous()];
      }

      this.setState({iteratorCount: this.iterator.current()})
      this.openModal(marker);
      this.animateToCoords(marker.latitude, marker.longitude);
    }
  }

  updateIterator(barID) {
    if (this.iterator !== null) {
      const index = this.findIndex(barID);
      this.setState({iteratorCount: index})
      this.iterator.update(index);
      this.openModal(this.props.route.params.markers[index]);
    }
  }

  renderModal = () => (

              <View style={styles.modalContent}>
                <View>
                  <View style={styles.info}>
                    <View style={styles.leftSide}>
                      <Text style={styles.bigBarInfo}>{this.state.bar.barName}</Text>
                      <Text>{this.state.bar.address.street + ", " + this.state.bar.address.buildingNumber}</Text>
                      <Text>{this.state.bar.address.postal_code + ", " + this.state.bar.address.city}</Text>
                    </View>

                    {this.state.bar.fmdRating ? (
                        <View style={styles.rightSide}>
                          <Text style={styles.bigBarInfo}>FMD Rating</Text>
                          <Text style={[styles.bigBarInfo, {
                            fontSize: 27,
                            color: "#09D4FF"
                          }]}>{this.state.bar.fmdRating}</Text>
                        </View>
                    ) : (null)}

                  </View>

                  <TouchableOpacity style={styles.drinkShortcut} onPress={() => this.showShortcutDrink()}>
                    <View style={styles.shortcutID}>
                      <Text style={{fontSize: 15, fontWeight: "bold", marginLeft: 2}}>{this.state.bar.drinkName}</Text>
                      <View>
                        <AirbnbRating
                            defaultRating={this.state.bar.drinkRating}
                            showRating={false}
                            size={15}
                            count={5}
                            isDisabled={true}
                        />
                      </View>
                    </View>
                    <View style={styles.shortcutPrice}>
                      <Text>{this.state.bar.price + " Kč"}</Text>
                    </View>
                  </TouchableOpacity>

                </View>
                <Button color={"white"} style={styles.goToProfile} onPress={() => this.showBarScreen()}>
                  <Text style={{color: "black"}}>Visit bar</Text>
                </Button>
              </View>
  )

  async showBarScreen() {
    this.props.navigation.navigate('BarProfile', {id: this.state.bar.barId, drinkId: this.state.bar.drinkMenuId})
  }

  async showShortcutDrink() {
    this.props.navigation.navigate('DrinkProfile', {barId: this.state.bar.barId, drinkId: this.state.bar.drinkMenuId})
  }

  animateToCoords(lat, long) {
    this.map.animateToRegion({
      latitude: lat,
      longitude: long,
      latitudeDelta: 0.0011,
      longitudeDelta: 0.0021
    }, 2000)
  }


  render() {

    return (
        <View style={styles.container}>

          <StatusBar
              animated={true}
              backgroundColor={"transparent"}
              barStyle="dark-content"
              translucent={true}
          />

          {this.state.isMapLoaded ? (
              <>
                {this.state.iteratorCount !== null ? (
                    <View style={{paddingBottom: 10, alignItems: "flex-end", display:"flex", flexDirection: "row", justifyContent: "space-between", zIndex: 1, backgroundColor: "white", elevation: 5, position: "absolute", top: 0, width: "100%", height: 100}}>

                        <TouchableOpacity onPress={() => {
                          this.iterateThroughMarkers(false)
                        }}>
                          <Icon name={"chevron-back-outline"} size={30}/>
                        </TouchableOpacity>

                          <View style={styles.iterationCount}>
                            <Text style={{fontSize: 23, color: "#09D4FF"}}>{(this.state.iteratorCount + 1)}</Text>
                            <Text style={{fontSize: 23}}>{"/" + (this.state.maxCount + 1)}</Text>
                          </View>

                          <TouchableOpacity onPress={() => {
                            this.iterateThroughMarkers(true)
                          }}>
                            <Icon name={"chevron-forward-outline"} size={30}/>
                          </TouchableOpacity>

                    </View>

                ) : (null)}


                <MapView
                    customMapStyle={getMap()}
                    ref={ref => {
                      this.map = ref;
                    }}
                    showsMyLocationButton={false}
                    provider={PROVIDER_GOOGLE}
                    style={styles.map}
                    showsUserLocation={true}
                    toolbarEnabled={false}
                    showsPointsOfInterest={false}
                    initialRegion={this.state.region}
                    followsUserLocation={false}
                >

                  {this.state.favoriteBar !== null ? (
                    <Marker pinColor={"red"} coordinate={{latitude: this.props.route.params.favoriteBar.coordinates[1], longitude: this.props.route.params.favoriteBar.coordinates[0]}}
                      onPress={() => this.openModal(this.state.favoriteBar)}
                    >
                    </Marker>
                  ): (null)
                  }

                  {
                    this.props.route.params.markers.map((marker: any) => (
                        <Marker
                            key={marker.barId}
                            pinColor={"rgb(0," + (this.findIndex(marker.barId) * 30 )  + ",255)"}
                            coordinate={{
                              latitude: parseFloat(marker.latitude),
                              longitude: parseFloat(marker.longitude)
                            }}
                            onPress={() => {
                              this.updateIterator(marker.barId)
                            }}
                        >
                        </Marker>
                    ))}

                </MapView>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.animate()}
                >
                  <Icon color={"red"} name={"location-sharp"} size={30}/>
                </TouchableOpacity>

                <BottomSheet
                    ref={ref => {
                      this.modal = ref
                    }}
                    initialSnap={1}
                    snapPoints={[250, 0]}
                    renderContent={this.renderModal}
                />
              </>
          ) : (
              <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                <Spinner type={"Wave"} style={styles.loading} size={40}/>
              </View>
          )}
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    backgroundColor: "white"
  },
  loading: {
    position: "absolute",
    top: "40%",
    color: "#77E7FF"
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  button: {
    backgroundColor: "white",
    padding: 10,
    position: "absolute",
    right: "5%",
    top: "17.5%",
    borderRadius: 100,
    elevation: 5
  },

  iterationCount: {
    display: "flex",
    flexDirection: "row",
    justifyContent: 'center',
    alignItems: "center"
  },


  modalContent: {
    height: "100%",
    backgroundColor: "white",
    paddingTop: 5,
    elevation: 5
  },

  info: {
   paddingHorizontal: "2.5%",
    marginTop: "1.5%",
    display: "flex",
    flexDirection: "row"
  },

  bigBarInfo: {
    fontSize: 20,
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold")
  },

  rightSide: {
   alignSelf: "flex-start",
    flex: 0.5,
    alignItems: "flex-end"

  },

  leftSide: {
    flex: 0.5,
  },

  goToProfile: {
    alignSelf: "center",
    backgroundColor: "#77E7FF",
    position: "absolute",
    bottom: "6.5%",
    width: "60%",
    paddingVertical: 3,
    borderRadius: 10

  },

  drinkShortcut: {
    marginTop: "3.5%",
    marginBottom: "10%",
    display: "flex",
    flexDirection: "row",
    marginHorizontal: "2.5%"
  },

  shortcutID: {
    flex: 0.8,
    justifyContent: "center",
    alignItems: "flex-start",
    //flexDirection: "column"
  },

  shortcutPrice: {
    flex: 0.2,
    alignItems: "flex-end",
    justifyContent: "center"
  }


});



