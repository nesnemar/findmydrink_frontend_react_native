import React,{Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions, TextInput,
  Image,
  Keyboard,
  ImageBackground,
    TouchableHighlight
} from 'react-native';

import {SafeAreaView} from "react-native-safe-area-context";

import Slider from "react-native-smooth-slider";

import GetLocation from 'react-native-get-location';

import AsyncStorage from "@react-native-community/async-storage";

import getLocation from "../FunctionsWorkingWithAPI/getLocation";

import { Button } from "react-native-paper";
import { Card, CardItem} from "native-base";
import axios from "axios";

import {remote} from "../herokuAddress";
import {BlurView} from 'expo-blur';
import Icon from 'react-native-vector-icons/Ionicons';



export default class SearchScreen extends Component {

  state = {
    distance: 0,
    rating: 0,
    price: 0,
    latitude: null,
    longitude: null,

    mode: "DEFAULT",

    itemInSearch: "",
    choices: [],
    chosenId: null,
    itemChosen: false
  };


  async getChoices() {

    await axios.post(remote + "/api/search/whisper", {crumb: this.state.itemInSearch}, {headers: {"Authorization": await AsyncStorage.getItem("AccessToken")}})
        .then(async (response) => {

          console.log(response.data)

          await this.setState({
            choices: response.data
          })
        })
        .catch(async (error) => {

          console.log(error);
          await this.setState({
            choices: []
          })
        })
  }


  async search() {

    const markers = [];

    let Location = await getLocation();

    if (!Location) {
      return;
    }

    await axios.post(remote + '/api/search/search', {
      drink_id: this.state.chosenId,
      priorities: {price: this.state.price, distance: this.state.distance, rating: this.state.rating},
      location: {latitude: Location.latitude, longitude: Location.longitude}
    }, {headers: {Authorization: await AsyncStorage.getItem("AccessToken")}})
        .then((response) => {
          console.log(response.data)

          response.data.forEach(item => {
            markers.push({
              barId: item.barId,
              latitude: item.location[1],
              longitude: item.location[0],
              drinkMenuId: item.drinkMenuId,
              fmdRating:
                  Math.round(item.fmdRating.toFixed(2) * 100)
            })
          })

          this.props.navigation.navigate('Root', {screen: 'Home', params: {markers: markers, markersObtained: "SENT"}});

        })
        .catch(error => {
          console.error(error);
        });

  }


  render() {
    return (
        <SafeAreaView style={styles.container} keyboardShouldPersistTaps={"always"}>

          <StatusBar
              animated={true}
              backgroundColor={"transparent"}
              barStyle="dark-content"
              translucent={true}

          />

          <View style={styles.searchInput}>
            <TextInput
                value={this.state.itemInSearch}
                onFocus={() => {
                  {
                    this.state.mode !== "SEARCH" ? (
                        this.setState({mode: "SEARCH"})
                    ) : (null)
                  }
                }}
                onChangeText={async (itemInSearch) => {
                  await this.setState({itemInSearch, itemChosen: false, chosenId: null})
                  if (this.state.itemInSearch.trim().length >= 3) {
                    this.getChoices();
                  } else {
                    await this.setState({choices: []})
                  }
                }}
                placeholder='Type your favorite drink...'
                placeholderTextColor='grey'
                style={[styles.input, {flex: this.state.mode === "SEARCH" ? 0.70 : 0.9}]}
            />

            {this.state.mode === "SEARCH" ? (
                <TouchableOpacity style={{flex: 0.2}} color={"white"} onPress={() => {
                  this.setState({mode: "DEFAULT"})
                  Keyboard.dismiss();
                }}>
                  <Text style={styles.cancelButton}>Cancel</Text>
                </TouchableOpacity>
            ) : (null)}

          </View>


          {this.state.mode === "DEFAULT" ? (
              <>
                <View style={styles.sliderContainer}>
                  <View style={styles.sliders}>
                    <Slider style={styles.slider}
                            maximumValue={100}
                            minimumValue={0}
                            maximumTrackTintColor='#F5F5F5'
                            minimumTrackTintColor={"#00C4EE"}
                            value={this.state.distance}
                            trackStyle={styles.track}
                            thumbStyle={styles.thumb}
                            onSlidingComplete={(distance) => this.setState({distance})}
                            step={1}
                    />
                    <Text style={{position: "absolute", top: 10, fontSize: 15,fontWeight: Platform.OS === "ios" ? ("800") : ("bold")}}>{"Distance: " + this.state.distance + "%"}</Text>
                  </View>
                  <View style={styles.sliders}>
                    <Slider style={styles.slider}
                            maximumValue={100}
                            minimumValue={0}
                            maximumTrackTintColor='#F5F5F5'
                            minimumTrackTintColor={"#77E7FF"}
                            value={this.state.rating}
                            trackStyle={styles.track}
                            thumbStyle={styles.thumb}
                            onSlidingComplete={(rating) => this.setState({rating})}
                            step={1}
                    />
                    <Text style={{position: "absolute", top: 10, fontSize: 15,fontWeight: Platform.OS === "ios" ? ("800") : ("bold")}}>{"Rating: " + this.state.rating + "%"}</Text>
                  </View>
                  <View style={styles.sliders}>
                    <Slider
                        style={styles.slider}
                        maximumValue={100}
                        minimumValue={0}
                        value={this.state.price}
                        maximumTrackTintColor='#F5F5F5'
                        minimumTrackTintColor={"#09D4FF"}
                        trackStyle={styles.track}
                        thumbStyle={styles.thumb}
                        onSlidingComplete={(price) => this.setState({price})}
                        step={1}
                    />
                    <Text style={{position: "absolute", top: 10, fontSize: 15,fontWeight: Platform.OS === "ios" ? ("800") : ("bold")}}>{"Price: " + this.state.price + "%"}</Text>
                  </View>
                </View>

                <Button
                    style={[styles.button, {backgroundColor: this.state.itemChosen && this.state.chosenId ? ("#77E7FF") : ("#EBEBEB")}]}
                    color={"white"} onPress={(e) => {
                  if (this.state.chosenId && this.state.itemChosen) {
                    this.search();
                  } else {
                    e.preventDefault();
                  }
                }}>
                  <Text style={{color: "black"}}>Search</Text>
                </Button>

              </>

          ) : (
              <ScrollView contentContainerStyle={{display: "flex", flexDirection: "column", alignItems: "center"}} style={styles.renderedCardCont}
                          keyboardShouldPersistTaps={"always"}>

                <Card transparent={true} style={{width: "90%"}}>
                {this.state.choices.map((item: any) => (
                    <TouchableHighlight
                        underlayColor={"red"}
                        style={{
                          marginTop: 10,
                          elevation: 3,
                          borderRadius: 15
                        }}
                        key={item._id}
                        onPress={() => {
                          Keyboard.dismiss();
                          this.setState({
                            mode: "DEFAULT",
                            itemInSearch: item.name,
                            itemChosen: true,
                            chosenId: item._id
                          })
                        }}
                    >
                      <View style={styles.renderedCard}>
                        <ImageBackground imageStyle={{borderRadius: 15}} style={{flex: 0.2, height: 50, width: 50}} source={require("../Resources/beer.jpg")}/>
                        <Text style={styles.name}>{item.name}</Text>
                      </View>
                    </TouchableHighlight>
                ))}
                </Card>
              </ScrollView>
          )}

        </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    display: "flex",
    alignItems: "center"
  },

  button: {
    backgroundColor: "#77E7FF",
    width: "60%",
    paddingVertical: 3,
    position: "absolute",
    bottom: "2%",
    borderRadius: 10,
  },

  cancelButton: {
    fontSize: 17,
    color: "red",
    fontWeight: "bold",
    textAlign: "center"
  },

  searchInput: {
    marginTop: 75,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },

  input: {
    fontFamily: 'Baskerville',
    fontSize: 18,
    backgroundColor: "#EBEBEB",
    borderRadius: 5,
    paddingHorizontal: 10
  },
  sliderContainer: {
    marginTop: "10%",
    textAlign: "center"
  },
  sliders: {
    marginVertical: "3%",
    alignItems: "center"
  },
  slider: {
    marginBottom: "6%"
  },
  track:{
    height: 50,
    width: Dimensions.get("window").width * 0.8,
    borderRadius: 18
  },
  thumb:{
    width: 0,
    height: 0,
    borderRadius: 18
  },

  renderedCardCont: {
    marginTop: "5%",
    width: "100%"
  },
  renderedCard:{
    backgroundColor: "white",
    paddingHorizontal: "5%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 15,
    paddingBottom: 15,
    borderRadius: 15

  },

  name: {
    fontWeight: "bold",
    fontSize: 16,
    flex: 0.8
  }

});
