import React,{Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar
} from "react-native";


import {SafeAreaView} from "react-native-safe-area-context";

import { Button } from "react-native-paper";
import AsyncStorage from "@react-native-community/async-storage";

import {AuthContext} from "../../AuthContext";
import { Card, CardItem} from "native-base";


export default class SettingsScreen extends Component {

  async logOut(context){
    if(await AsyncStorage.getItem("AccessToken")){
      await AsyncStorage.removeItem("AccessToken")
      context.toggleLogged(false)
    }

  }


  render() {
    return (
      <AuthContext.Consumer>
        {(context) => (
          <SafeAreaView style={styles.container}>

            <StatusBar
              animated={true}
              backgroundColor={"transparent"}
              barStyle="dark-content"
              translucent={true}

            />

            <View style={styles.settingsContainer}>

                <Card transparent={true} style={styles.settings}>
                  <Text style={styles.cardText}>Localization</Text>
                  <CardItem style={styles.cardItem} button onPress={() => console.log("press")}>
                    <Text style={styles.buttonText}>Language</Text>
                  </CardItem>
                </Card>

                <Card transparent={true} style={styles.settings}>
                  <Text style={styles.cardText}>Info</Text>
                  <CardItem style={styles.cardItem} button onPress={() => console.log("press")}>
                    <Text style={styles.buttonText}>Terms of use</Text>
                  </CardItem>
                  <CardItem style={styles.cardItem} button onPress={() => this.props.navigation.navigate("CustomerSupport")}>
                    <Text style={styles.buttonText}>Customer support</Text>
                  </CardItem>
                  <CardItem style={styles.cardItem} button onPress={() => this.props.navigation.navigate("About")}>
                    <Text style={styles.buttonText}>About</Text>
                  </CardItem>
                </Card>

                <Card transparent={true} style={styles.settings}>
                  <Text style={styles.cardText}>Account settings</Text>
                  <CardItem style={styles.cardItem} button onPress={() => console.log("press")}>
                    <Text style={styles.accountButtonText}>Deactivate account</Text>
                  </CardItem>
                  <CardItem style={styles.cardItem} button onPress={() => this.logOut(context)}>
                    <Text style={styles.accountButtonText}>Logout</Text>
                  </CardItem>
                </Card>

            </View>

          </SafeAreaView>
        )
        }
      </AuthContext.Consumer>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    display: "flex"
  },
  options: {
    backgroundColor: "white",
    left: 0
  },

  settingsContainer: {
  },

  cardText: {
    marginTop: "5%",
    paddingVertical: "2.5%",
    marginLeft: "5%",
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold"),
    fontSize: 16
  },

  buttonText: {
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold")
  },

  accountButtonText: {
    color: "red",
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold")
  },

  settings:{

  },
  cardItem: {
    paddingLeft: "5%"
  }

});
