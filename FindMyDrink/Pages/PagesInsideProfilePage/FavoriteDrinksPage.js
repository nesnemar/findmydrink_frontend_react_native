import React,{Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar, Dimensions,
  RefreshControl, Image, ImageBackground, TouchableOpacity,
    TouchableHighlight
} from 'react-native';

import {SafeAreaView} from "react-native-safe-area-context";
import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";
import {Card, CardItem, Spinner} from 'native-base';
import Icon from "react-native-vector-icons/Ionicons";

import {remote} from "../../herokuAddress";


export default class FavoriteDrinksScreen extends Component {

  state: {
    favoritesLoaded: null,
    drinks: null,
    refreshing: false
  }



  constructor(props) {
    super(props);
    this.state = {
      drinks: null,
      favoritesLoaded: "LOADING",
      refreshing: false
    }
  }

  async componentDidMount() {
    await this.getDrinkInfo();
  }

  async getDrinkInfo(){

    let data = [];

    await axios.post(remote + '/api/userprofile/favdrinksglobal', {}, { headers: { "Authorization": await AsyncStorage.getItem("AccessToken") } })
      .then((response) => {

        data = response.data;

        console.log(data);

        this.setState({
          favoritesLoaded: "OK",
          drinks: data,
        })

      })
      .catch(async(error) => {
        await this.setState({
          favoritesLoaded: "ERROR"
        })
      })
  }


  showBars(drinkId){
    this.props.navigation.navigate('FavoriteBars',{drinkId : drinkId})
  }


  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.favoritesLoaded === "OK" ? (
        <ScrollView
            refreshControl={
              <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={() => {
                    this.setState({refreshing: true})
                    setTimeout(() => {
                      this.setState({refreshing: false})
                    }, 500);
                    this.getDrinkInfo();
                  }
                  }
              />
        }>

        <StatusBar
          animated={true}
          backgroundColor={"transparent"}
          barStyle="dark-content"
          translucent={true}

        />

        <View>
          {Object.keys(this.state.drinks).length !== 0 ? (
          <Card transparent={true}>

            {Object.entries(this.state.drinks).map((category: any) => (
                  <View style={{marginBottom: 10, marginTop: 15, display: "flex", flexDirection: "column", alignItems: "center"}} key={category[0]}>
                    <Text style={styles.category}>{category[0]}</Text>
                    {category[1].map((drink) => (
                        <TouchableHighlight
                            underlayColor={"red"}
                            style={{borderRadius: 15, marginTop: 10, elevation: 3}}
                            key={drink.globalDrinkId}  button onPress={() => this.showBars(drink.globalDrinkId)}>
                          <View style={styles.cardItem}>
                            <ImageBackground imageStyle={{borderRadius: 15}} style={{flex: 0.2, height: 50, width: 50}} source={require("../../Resources/beer.jpg")}/>
                            <Text style={styles.drinkID}>{drink.drinkName}</Text>
                            <Icon style={{flex: 0.1}} size={25} name={"chevron-forward-outline"}/>
                          </View>
                        </TouchableHighlight>
                    ))}
                  </View>
              ))}
          </Card>

              ) : (
              <Text style={{textAlign: "center", fontSize: 15, fontWeight: Platform.OS === "ios" ? ("800") : ("bold")}}>
                No favorites added yet... Add few ;)
              </Text>
              )}

        </View>
        </ScrollView>
          ):(
            <View style={{flex: 1,display: "flex", alignItems:"center", justifyContent:"center"}}>
              <Spinner color={"black"}/>
            </View>
          )}
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },

  category: {
    fontSize: 20,
    alignSelf: "flex-start",
    paddingHorizontal: "5%",
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold")

  },

  cardItem: {
    backgroundColor: "white",
    paddingHorizontal: "5%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 15,
    width: "90%",
    paddingBottom: 15,
    borderRadius: 15
  },
  drinkID: {
    fontWeight: "bold",
    fontSize: 16,
    flex: 0.7
  }
});
