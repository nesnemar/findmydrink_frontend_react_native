import React,{Component} from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    RefreshControl, ImageBackground,
    TouchableHighlight
} from 'react-native';

import {SafeAreaView} from "react-native-safe-area-context";
import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";
import {Card, CardItem, Spinner} from 'native-base';
import {remote} from "../../herokuAddress";
import {AirbnbRating} from 'react-native-ratings';
import Icon from 'react-native-vector-icons/Ionicons';

export default class FavoriteBarsScreen extends Component {


    state: {
        bars: null,
        barsLoaded: null,
        refreshing: false
    }

    constructor(props) {
        super(props);
        this.state = {
            bars: null,
            barsLoaded: "LOADING",
            refreshing: false
        }
    }


    async componentDidMount() {
        await this.getFavoriteBars();
    }


    async getFavoriteBars(){

        let data = [];

        await axios.post(remote + '/api/userprofile/favdrinksbars', {drinkId: this.props.route.params.drinkId}, { headers: { "Authorization": await AsyncStorage.getItem("AccessToken") } })
            .then((response) => {

                data = response.data;

                console.log(data);

                this.setState({
                    barsLoaded: "OK",
                    bars: data,
                })

            })
            .catch(async(error) => {
                await this.setState({
                    barsLoaded: "ERROR"
                })
            })
    }

    showBar(bar){
        this.props.navigation.navigate('Home',{favoriteBar : bar, favoriteBarObtained: "SENT"})
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>
                {this.state.barsLoaded === "OK" ? (
                    <ScrollView
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={() => {
                                    this.setState({refreshing: true})
                                    setTimeout(() => {
                                        this.setState({refreshing: false})
                                    }, 500);
                                    this.getFavoriteBars();
                                }
                                }
                            />
                        }>

                        <StatusBar
                            animated={true}
                            backgroundColor={"transparent"}
                            barStyle="dark-content"
                            translucent={true}

                        />

                        <View>
                            <Card transparent={true} style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
                                {this.state.bars.map((bar: any) => (
                                    <TouchableHighlight underlayColor={"red"}
                                                        style={{borderRadius: 15, marginTop: 10, elevation: 3}} key={bar.barId} button onPress={() => this.showBar(bar)}>
                                        <View style={styles.cardItem}>
                                            <ImageBackground imageStyle={{borderRadius: 15}} style={{flex: 0.2, height: 50, width: 50}} source={require("../../Resources/bar.jpg")}/>
                                            <Text style={styles.name}>{bar.barName}</Text>
                                            <Icon color={"red"} style={{flex: 0.1}} size={30} name={"location-sharp"}/>
                                        </View>
                                    </TouchableHighlight>
                                ))}
                            </Card>
                        </View>
                    </ScrollView>
                ):(
                    <View style={{flex: 1,display: "flex", alignItems:"center", justifyContent:"center"}}>
                        <Spinner color={"black"}/>
                    </View>
                )}
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },

    cardItem: {
        backgroundColor: "white",
        paddingHorizontal: "5%",
        display: "flex",
        alignItems: "center",
        width: "90%",
        flexDirection: "row",
        paddingTop: 15,
        paddingBottom: 15,
        borderRadius: 15,
    },
    name: {
        fontWeight: "bold",
        fontSize: 16,
        flex: 0.7
    }
});
