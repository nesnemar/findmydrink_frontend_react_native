import React,{Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {SafeAreaView} from "react-native-safe-area-context";

import { Button } from "react-native-paper";



export default class AboutScreen extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>

        <StatusBar
          animated={true}
          backgroundColor={"transparent"}
          barStyle="dark-content"
          translucent={true}

        />

        <Text>About</Text>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  }
});
