import React,{Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Modal,
  ImageBackground, TouchableHighlight,
  Alert,
  Image, Dimensions,
} from 'react-native';

import {Input} from "react-native-elements";

import {AuthContext} from "../../../AuthContext";

import AsyncStorage from "@react-native-community/async-storage";

import axios from "axios";


import { Button } from "react-native-paper";
import Icon from "react-native-vector-icons/Ionicons";


import {SafeAreaView} from "react-native-safe-area-context";

import AwesomeAlert from 'react-native-awesome-alerts';
import CustomAlert from "../../../CustomAlert";

import {remote} from "../../../herokuAddress"



export default class LoginScreen extends Component {


  state = {
    username: '',
    password: '',

    errorMessage: null
  };

  async sendLoginForm(context) {


    let username = this.state.username.trim();
    let password = this.state.password.trim();


    if (!username) {

      await this.setState({ errorMessage: "Please fill Email input field!"})

    } else if (!password) {

      await this.setState({ errorMessage: "Please fill Password input field!" })


    } else {


      await axios.post(remote + '/api/users/loginuser', {
        username: username,
        password: password
      })

        .then(async (response) => {

          if(response.headers.authorization){
            await AsyncStorage.setItem("AccessToken", response.headers.authorization);

            if (await AsyncStorage.getItem("AccessToken") && (!context.loggedIn) && (response.status === 200)) {
              context.toggleLogged(true)
            }
          }



        })
        .catch(error => {
          console.error(error);

          if(error.request.status === 401 || error.request.status === 404){
            this.setState({errorMessage: "Username or password is incorrect!"})
          }

        });



    }

  }


  render() {

    return (
      <AuthContext.Consumer>
        {(context) => (
          <SafeAreaView style={styles.container} keyboardShouldPersistTaps={"always"}>

            <StatusBar
              animated={true}
              backgroundColor={"transparent"}
              barStyle="dark-content"
              translucent={true}

            />

            <View style={styles.IMGCont}>
              <Image source={require("../../../Resources/logo2.png")} style={styles.tinyLogo}/>
            </View>

            <View style={styles.inputs}>
              <Input
                value={this.state.username}
                onChangeText={(username) => this.setState({ username })}
                placeholder='Username'
                placeholderTextColor='grey'
                inputContainerStyle={styles.input}
                errorStyle={{display: "none"}}
              />
              <Input
                value={this.state.password}
                onChangeText={(password) => this.setState({ password })}
                placeholder={'Password'}
                secureTextEntry={true}
                placeholderTextColor='grey'
                inputContainerStyle={styles.input}
                errorStyle={{display: "none"}}
              />
            </View>

            <Button style={styles.login} color={"#00DAF3"} onPress={() => this.sendLoginForm(context)}>
              <Text color={"white"}>Log in</Text>
            </Button>

            <View style={styles.icons}>
              <TouchableHighlight style={{marginRight: 10}}>
                <Icon size={30} color={"black"} backgroundColor={"white"} name={"logo-google"}/>
              </TouchableHighlight>
              <TouchableHighlight style={{marginLeft: 10}}>
                <Icon size={30} color={"black"} backgroundColor={"white"} name={"logo-facebook"}/>
              </TouchableHighlight>
            </View>

            <Button style={styles.redirectAuth} color={"white"}
                    onPress={() => this.props.navigation.navigate("Register")}>
              <Text style={{color: "black"}}>Register</Text>
            </Button>

            <AwesomeAlert
                show={this.state.errorMessage ? (true) : (false)}
                customView={CustomAlert(this.state.errorMessage)}
                closeOnHardwareBackPress={false}
                closeOnTouchOutside={false}
                confirmText={"OK"}
                showConfirmButton={true}
                onConfirmPressed={() => this.setState({errorMessage: null})}
            />


          </SafeAreaView>
        )
        }
      </AuthContext.Consumer>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white",
    display: "flex"
  },
  header: {
    marginVertical: 5,
    fontSize: 25,
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold")
  },

  IMGCont: {
    marginTop: "10%",
    height: "20%",
    alignItems: "center"
  },

  tinyLogo: {
    width: '70%',
    height: undefined,
    aspectRatio: 1,
  },

  inputs:{
    marginTop: "35%",
    width: "95%"
  },
  input: {
    fontSize: 18,
    height: 50,
    marginVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: "#EBEBEB",
    borderRadius: 5,
    borderBottomWidth: 0

  },
  icons: {
    marginTop: "10%",
    display: "flex",
    flexDirection: "row"
  },
  login: {
    width: "90%",
    marginTop: "5%",
    padding: 5,
    borderRadius: 10,
    borderWidth: 1.5,
    borderColor: "#00DAF3",
    backgroundColor: "transparent"
  },
  redirectAuth: {
    bottom: "2.5%",
    position: "absolute",
    borderRadius: 10
  },


  modal: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },

  modalContent: {
    height: "25%",
    width: "90%",
    backgroundColor: "white",
    position: "absolute",
    bottom: 10,
    borderRadius: 20,
    padding: 35,
    alignItems: "center", //   elevation: 5


  }
});
