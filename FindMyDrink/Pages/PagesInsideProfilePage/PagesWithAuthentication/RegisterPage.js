import React,{Component} from 'react';

import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,
  ImageBackground, TouchableHighlight, Image,
} from 'react-native';

import {Input} from "react-native-elements";

import { Button } from "react-native-paper";
import AsyncStorage from "@react-native-community/async-storage";

import Icon from "react-native-vector-icons/Ionicons";

import {AuthContext} from "../../../AuthContext";
import axios from "axios";
import {SafeAreaView} from "react-native-safe-area-context";
import CustomAlert from '../../../CustomAlert';
import AwesomeAlert from 'react-native-awesome-alerts';

import {remote} from "../../../herokuAddress"



export default class RegisterScreen extends Component {

  state = {
    email: '',
    username: "",
    password: '',
    confirm: "",

    errorMessage: null
  };


  async sendRegisterForm(context) {

    let username = this.state.username.trim();
    let email = this.state.email.trim();
    let password = this.state.password.trim();
    let confirm = this.state.confirm.trim();

     if(!email) {

      await this.setState({errorMessage: "Please fill Email input field!"})
    }

    else if (!email.includes("@")) {

      await this.setState({errorMessage: "Please enter valid Email!"})

    }

    else if(!username) {

      await this.setState({errorMessage: "Please fill Username input field!"})

    }
    else if (!password) {

      await this.setState({errorMessage: "Please fill Password input field!"})

    } else if(password != confirm) {

       await this.setState({errorMessage: "Your passwords do not match!"})
     }

     else {

      //S6
      await axios.post(remote + '/api/users/createuser', {
        email: email,
        username: username,
        password: password
      })

        .then(async (response) => {

          console.log(response)

          if (response.headers.authorization) {
            await AsyncStorage.setItem("AccessToken", response.headers.authorization);
          } else {
            await AsyncStorage.remove("AccessToken");
            context.toggleLogged(true);
          }

        })
        .catch(error => {
          console.error(error);
        });


      if (await AsyncStorage.getItem("AccessToken") && (!context.loggedIn)) {
        context.toggleLogged(true)
      }

  }
  }

  render() {
    return (
      <AuthContext.Consumer>
        {(context) => (
          <SafeAreaView style={styles.container} keyboardShouldPersistTaps={"always"}>

            <StatusBar
              animated={true}
              backgroundColor={"transparent"}
              barStyle="light-content"
              translucent={true}

            />


            <View style={styles.IMGCont}>
              <Image source={require("../../../Resources/logo2.png")} style={styles.tinyLogo}/>
            </View>

            <View style={styles.inputs}>
            <Input
              value={this.state.email}
              keyboardType='email-address'
              onChangeText={(email) => this.setState({ email })}
              placeholder='Example@example.com'
              placeholderTextColor='grey'
              inputContainerStyle={styles.input}
              errorStyle={{display: "none"}}
            />
            <Input
              value={this.state.username}
              onChangeText={(username) => this.setState({ username })}
              placeholder='Username'
              placeholderTextColor='grey'
              inputContainerStyle={styles.input}
              errorStyle={{display: "none"}}
            />
            <Input
              value={this.state.password}
              onChangeText={(password) => this.setState({ password })}
              placeholder={'Password'}
              secureTextEntry={true}
              placeholderTextColor='grey'
              inputContainerStyle={styles.input}
              errorStyle={{display: "none"}}
              inputStyle={{
                fontFamily: "monospace"
              }}
            />
            <Input
              value={this.state.confirm}
              onChangeText={(confirm) => this.setState({ confirm })}
              placeholder={'Confirm password'}
              secureTextEntry={true}
              placeholderTextColor='grey'
              inputContainerStyle={styles.input}
              errorStyle={{display: "none"}}
            />
          </View>
            <Text style={styles.errorMessage}>{this.state.errorMessage}</Text>

            <Button color={"#00DAF3"} style={styles.register} onPress={() => this.sendRegisterForm(context)}>
              <Text color={"white"}>Register</Text>
            </Button>

            <View style={styles.icons}>
              <TouchableHighlight style={{marginRight: 10}}>
                <Icon size={30} color={"black"} backgroundColor={"white"} name={"logo-google"}/>
              </TouchableHighlight>
              <TouchableHighlight style={{marginLeft: 10}}>
                <Icon size={30} color={"black"} backgroundColor={"white"} name={"logo-facebook"}/>
              </TouchableHighlight>
            </View>


            <Button style={styles.redirectAuth} color={"white"}
                    onPress={() => this.props.navigation.pop()}>
              <Text style={{color: "black"}}>Log in</Text>
            </Button>


            <AwesomeAlert
                show={this.state.errorMessage ? (true) : (false)}
                customView={CustomAlert(this.state.errorMessage)}
                closeOnHardwareBackPress={false}
                closeOnTouchOutside={false}
                confirmText={"OK"}
                showConfirmButton={true}
                onConfirmPressed={() => this.setState({errorMessage: null})}
            />


          </SafeAreaView>
        )
        }
      </AuthContext.Consumer>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display:"flex",
    alignItems: "center",
    backgroundColor: "white"
  },

  IMGCont: {
    marginBottom: "10%",
    height: "20%",
    alignItems: "center"
  },

  tinyLogo: {
    width: '70%',
    height: undefined,
    aspectRatio: 1,
  },

  inputs:{
    marginTop: "15%",
    width: "95%"
  },
  input: {
    fontSize: 18,
    height: 50,
    marginVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: "#EBEBEB",
    borderRadius: 5,
    borderBottomWidth: 0

  },
  icons: {
    marginTop: "10%",
    display: "flex",
    flexDirection: "row"
  },
  register: {
    width: "90%",
    padding: 5,
    borderRadius: 10,
    borderWidth: 1.5,
    borderColor: "#00DAF3",
    backgroundColor: "transparent"
  },
  redirectAuth: {
    bottom: "2.5%",
    position: "absolute",
    borderRadius: 10

  },
  errorMessage: {
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold"),
    color: "red"
  }
});
