import React,{Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';


import { Button } from "react-native-paper";

import {SafeAreaView} from "react-native-safe-area-context";


export default class CustomerSupportScreen extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>

        <StatusBar
          animated={true}
          backgroundColor={"transparent"}
          barStyle="dark-content"
          translucent={true}

        />

        <Text>Customer support</Text>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  }
});
