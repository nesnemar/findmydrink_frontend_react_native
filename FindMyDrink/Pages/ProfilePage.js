import React,{Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    StatusBar,
    Image, TouchableOpacity,
    ImageBackground, ScrollView,
} from 'react-native';

import {SafeAreaView} from "react-native-safe-area-context";

import {CardItem,Card} from "native-base";
import Icon from 'react-native-vector-icons/Ionicons';
import {BlurView} from 'expo-blur';
import axios from 'axios';
import {remote} from '../herokuAddress';
import AsyncStorage from '@react-native-community/async-storage';




export default class ProfileScreen extends Component {

    state = {
        username: null
    }

    async componentDidMount() {
        await this.getUserData();
    }

    async getUserData(){
        await axios.get(remote + '/api/userprofile/getuserdata', { headers: { "Authorization": await AsyncStorage.getItem("AccessToken") } })
            .then(response => {
                console.log(response.data)
                this.setState({username: response.data})
            }).catch(error => {
                console.log(error)
            })
    }

  render() {
    return (
      <View style={styles.container}>

        <StatusBar
          animated={true}
          backgroundColor={"transparent"}
          barStyle="dark-content"
          translucent={true}
        />

        <View style={styles.graphics}>
            <ImageBackground style={{width: "100%", height: "100%"}} source={require("../Resources/zuck.jpg")}>

                <View style={{position: "absolute", bottom: 0, width: "100%"}}>
                    <Text style={styles.UserName}>{this.state.username}</Text>
                </View>
            </ImageBackground>
        </View>

        <View style={styles.options}>

            <TouchableOpacity style={styles.cardItem} onPress={() => console.log("pressed")}>
                <Icon size={30} color={"black"} name={"chatbox-outline"}/>
                <Text style={styles.cardItemText}>Reviews</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.cardItem} onPress={() => this.props.navigation.navigate("FavoriteDrinks")}>
                <Icon size={30} color={"red"} name={"heart"}/>
              <Text style={styles.cardItemText}>Favorites</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.cardItem} onPress={() => this.props.navigation.navigate("Settings")}>
                <Icon color={"black"} name={"settings-outline"} size={30}/>
                <Text style={styles.cardItemText}>Settings</Text>
            </TouchableOpacity>


        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },

  graphics: {
    height: 300
  },

  UserName: {
      fontWeight: Platform.OS === "ios" ? ("800") : ("bold"),
      fontSize: 20,
      paddingVertical: 15,
      textAlign: "center",
      backgroundColor: "white",
      borderTopRightRadius: 20,
      borderTopLeftRadius: 20

  },

  options: {
      marginTop: "2.5%",
      display: "flex",
      flexDirection: "row",
      width: "100%",
      justifyContent: "center",
      alignItems: "flex-end"
  },

  cardItem: {
      flex: 0.33,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      paddingHorizontal: "5%",
      paddingVertical: 10
  },

  cardItemText: {
    fontWeight: Platform.OS === "ios" ? ("800") : ("bold"),
    fontSize: 16
  }



});
